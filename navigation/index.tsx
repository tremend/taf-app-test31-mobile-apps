import * as React from 'react';
import { useEffect } from 'react';
import * as SplashScreen from 'expo-splash-screen';
import Login from '../screens/Authentication/Login';
import {useAuth} from '../contexts/AuthContext';
import ProfileFlow from '../screens/Profile';

import EventsFlow from "../screens/Events";

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
const RootNavigator = createBottomTabNavigator();

const RootNavigatorFlow = () => {
    const {isAuthenticated, isStoreDataFetched} = useAuth();

    useEffect(() => {
        setTimeout(() => {
            SplashScreen.hideAsync().then();
        }, 500)
    }, [isStoreDataFetched]);

    return (
        isAuthenticated ? (
            <RootNavigator.Navigator>
                <RootNavigator.Screen name="Events" component={EventsFlow} />
                <RootNavigator.Screen name="Profile" component={ProfileFlow} />
            </RootNavigator.Navigator>
        ) : (
            <Login />
        )
    )
}

export default RootNavigatorFlow;
