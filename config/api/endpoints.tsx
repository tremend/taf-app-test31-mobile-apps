const Endpoints = {
    authenticate: 'authenticate',
    account: 'account',
};

export default Endpoints;
