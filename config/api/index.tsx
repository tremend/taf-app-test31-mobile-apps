import {create} from 'apisauce';
import {AxiosRequestConfig} from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const api = create({
    baseURL: 'http://test31-backoffice-api.taf-app.tremend.ro/api/',
});

/**
 * Interceptors can be easily added here or added in a different interceptors folder
 *
 * Response interceptor: api.addResponseTransform / api.addAsyncResponseTransform
 * Request interceptor: api.addRequestTransform / api.addAsyncRequestTransform
 */

api.addAsyncRequestTransform(async (request: AxiosRequestConfig) => {
    const token = await AsyncStorage.getItem('@RNAuth:token');

    if (token) {
        request.headers.Authorization = `Bearer ${token}`;
    }
});

export default api;


import {createApi, fetchBaseQuery} from '@reduxjs/toolkit/query/react';

// initialize an empty api service that we'll inject endpoints into later as needed
export const emptyApi = createApi({
    baseQuery: fetchBaseQuery({
        baseUrl: 'http://test31-backoffice-api.taf-app.tremend.ro/api/',
        prepareHeaders: (headers, {getState}) => {
            // By default, if we have a token in the store, let's use that for authenticated requests
            // const token = (getState() as RootState).auth.token
            // if (token) {
            headers.set(
                'Authorization',
                'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImF1dGgiOiJST0xFX0FETUlOLFJPTEVfVVNFUiIsImV4cCI6MTYyNjc5MDE1NX0.TLY_OmuM1iXlWUwHRo1UbEEqMg8DbMxmVKuwMZMZKP9ZDZ75h2w296l4J35Nzo3QY2oIMpSwxuLDG1o47DoA9w',
            );
            // }
            return headers;
        },
    }),
    endpoints: () => ({}),
});
