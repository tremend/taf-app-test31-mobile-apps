import {DefaultTheme, DarkTheme} from 'react-native-paper';
import {
    DefaultTheme as NavDefaultTheme,
    DarkTheme as NavDarkTheme,
} from '@react-navigation/native';

const primary = !!'' ? '' : '#3783e7'

const theme = {
    ...DefaultTheme,
    ...NavDefaultTheme,
    roundness: 5,
    colors: {
        ...DefaultTheme.colors,
        ...NavDefaultTheme.colors,
        primary
    },
};

const darkTheme = {
    ...DarkTheme,
    ...NavDarkTheme,
    roundness: 5,
    colors: {
        ...DarkTheme.colors,
        ...NavDarkTheme.colors,
        primary
    },
};

export {theme, darkTheme};
