import {ApiOkResponse, ApiErrorResponse} from 'apisauce';

const request = async <T,>({
    endpoint,
    setIsLoading,
    showError = true,
}: any): Promise<T> => {
    return new Promise(async (resolve, reject) => {
        setIsLoading?.(true);
        try {
            const {data, ok, originalError}: ApiErrorResponse<T> | ApiOkResponse<T> =
                await endpoint;
            ok ? resolve(data) : reject(originalError);
        } catch (e) {
            if (showError) {
            }
            reject(e);
        } finally {
            setIsLoading?.(false);
        }
    });
};

export default request;
