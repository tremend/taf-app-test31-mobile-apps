import {NavigationContainer} from '@react-navigation/native';
import React, {useEffect} from 'react';
import 'react-native-gesture-handler';
import RootNavigatorFlow from './navigation';
import {AuthProvider} from './contexts/AuthContext';
import {Provider as PaperProvider} from 'react-native-paper';
import {theme, darkTheme} from './config/theme';
import * as SplashScreen from 'expo-splash-screen';
import {useColorScheme} from 'react-native';

export default function App() {
    let colorScheme = useColorScheme();

    useEffect(() => {
        SplashScreen.preventAutoHideAsync().then();
    }, []);

    return (
        <PaperProvider theme={colorScheme === 'dark' ? darkTheme : theme}>
            <AuthProvider>
                <NavigationContainer theme={colorScheme === 'dark' ? darkTheme : theme}>
                    <RootNavigatorFlow />
                </NavigationContainer>
            </AuthProvider>
        </PaperProvider>
    );
}
