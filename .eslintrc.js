module.exports = {
  root: true,
  extends: '@react-native-community',
  rules: {
    'prettier/prettier': [
      'warn',
      {
        singleQuote: true,
        semi: true,
        printWidth: 90,
        tabWidth: 4,
        useTabs: false,
        trailingComma: 'all',
        bracketSpacing: false,
      },
    ],
    // Indent with 4 spaces
    indent: ['error', 4],

    // Indent JSX with 4 spaces
    'react/jsx-indent': ['error', 4],

    // Indent props with 4 spaces
    'react/jsx-indent-props': ['error', 4],
  },
};
