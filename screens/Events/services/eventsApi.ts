import Endpoints from './endpoints';
import {emptyApi} from '../../../config/api';

const eventsApi = emptyApi.injectEndpoints({
    endpoints: (builder) => ({
        getEvents: builder.query<any, string>({
            query: () => `${Endpoints.events}`,
        }),
    }),
    overrideExisting: false,
});

export const {useGetEventsQuery} = eventsApi;
