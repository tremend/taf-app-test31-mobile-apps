const Endpoints = {
    events: 'events',
    eventLocations: 'event-locations',
};

export default Endpoints;
