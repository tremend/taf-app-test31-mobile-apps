import {useState} from 'react';
import api from '../../../config/api';
import request from '../../../services/utils';
import Endpoints from './endpoints';
import {EventLocationModel, EventModel} from '../types';

const useEvents = () => {
    const [isLoading, setIsLoading] = useState(true);

    const getAll = async (): Promise<EventModel[]> => {
        return request({endpoint: api.get(Endpoints.events), setIsLoading});
    };

    const getLocation = async (id: number): Promise<EventLocationModel> => {
        return request({
            endpoint: api.get(`${Endpoints.eventLocations}/${id}`),
            setIsLoading,
        });
    };

    return {
        isLoading,
        getAll,
        getLocation,
    };
};

export default useEvents;
