import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {Card, Paragraph, Title} from 'react-native-paper';
import {EventModel} from '../types';

const EventItem = ({item, onPress}: {item: EventModel; onPress: any}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Card>
                <Card.Cover source={{uri: 'https://picsum.photos/700'}} />
                <Card.Content>
                    <Title>{item.name}</Title>
                    <Paragraph>{item.description}</Paragraph>
                </Card.Content>
            </Card>
        </TouchableOpacity>
    );
};

export default EventItem;

const styles = StyleSheet.create({
    container: {
        marginBottom: 20,
    },
});
