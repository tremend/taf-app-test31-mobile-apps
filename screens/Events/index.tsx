import {createStackNavigator} from '@react-navigation/stack';
import * as React from 'react';
import EventDetails from './EventDetails';
import EventList from './EventList';

const Stack = createStackNavigator();

const EventsFlow = () => {
    return (
        <Stack.Navigator initialRouteName={'events'}>
            <Stack.Screen
                name="events"
                options={{title: 'Events'}}
                component={EventList}
            />
            <Stack.Screen name="event" component={EventDetails} />
        </Stack.Navigator>
    );
};

export default EventsFlow;
