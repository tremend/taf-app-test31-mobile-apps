import React, {useLayoutEffect} from 'react';
import {SafeAreaView, StyleSheet, View} from 'react-native';
import {Avatar, Button, Caption, Title} from 'react-native-paper';
import {useAuth} from '../../contexts/AuthContext';

const Profile = ({navigation}: any) => {
    const {signOut, user} = useAuth();



    const onSignOut = async () => {
        try {
            await signOut();
        } catch (e) {
            // Handle error
            console.log(e);
        }
    };

    return (
        <SafeAreaView>
            <View style={styles.profileContainer}>
                <Avatar.Text size={40} label={`${user?.firstName.charAt(0)}`} />
                <Title>
                    {user?.firstName} {user?.lastName}
                </Title>
                <Caption>{user?.email}</Caption>
            </View>

            <Button mode="text" onPress={onSignOut}>
                Sign Out
            </Button>
        </SafeAreaView>
    );
};

export default Profile;

const styles = StyleSheet.create({
    profileContainer: {
        alignItems: 'center',
        marginTop: 20,
    },
});
