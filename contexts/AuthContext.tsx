import React, {createContext, useContext, useEffect, useState} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import request from '../services/utils';
import Endpoints from '../config/api/endpoints';
import api from '../config/api';
import {UserModel, UserResponse} from './types';
import {EmailPassword} from '../screens/Authentication/types';

interface AuthContextData {
    isAuthenticated: boolean;
    user: UserModel | null;
    token: string | null;
    isLoading: boolean;
    isStoreDataFetched: boolean;
    signIn: any;
    signOut: any;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

const AuthProvider: React.FC = ({children}) => {
    const [user, setUser] = useState<UserModel | null>(null);
    const [token, setToken] = useState<string | null>(null);
    const [isLoading, setIsLoading] = useState(false);
    const [isStoreDataFetched, setIsStoreDataFetched] = useState(false);

    // Check if data is stored about the user
    useEffect(() => {
        getStoredData().then();
    }, []);

    const getStoredData = async () => {
        const localToken = (await AsyncStorage.getItem('@RNAuth:token')) as string | null;
        const localUser = await AsyncStorage.getItem('@RNAuth:user');

        if (localToken && localUser) {
            setToken(localToken);
            setUser(JSON.parse(localUser) as UserModel);
        }

        setIsStoreDataFetched(true);
    };

    const signIn = async (userData: EmailPassword) => {
        const response = await request<UserResponse>({
            endpoint: api.post(Endpoints.authenticate, {
                username: userData.email,
                password: userData.password,
            }),
            setIsLoading,
        });

        await AsyncStorage.setItem('@RNAuth:token', response.id_token);

        const userAccount = await getAccount();

        response.user_data = userAccount;

        await AsyncStorage.setItem('@RNAuth:user', JSON.stringify(userAccount));

        setUser(userAccount);
        setToken(response.id_token);
    };

    const getAccount = (): Promise<UserModel> => {
        return request<UserModel>({
            endpoint: api.get(Endpoints.account),
            setIsLoading,
        });
    };

    const signOut = async () => {
        await AsyncStorage.multiRemove(['@RNAuth:user', '@RNAuth:token'], () => {
            setUser(null);
            setToken(null);
        });
    };

    return (
        <AuthContext.Provider
            value={{
                isAuthenticated: !!token,
                isStoreDataFetched,
                user,
                token,
                isLoading,
                signIn,
                signOut,
            }}
        >
            {children}
        </AuthContext.Provider>
    );
};

const useAuth = () => {
    return useContext(AuthContext);
};

export {AuthProvider, useAuth};
